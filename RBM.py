import torch
import torch.nn.functional as F # Para usar linear.
import torchvision as tv

################################################################
#		Clase RBM
################################################################


class RBM( torch.nn.Module):
    def __init__( self, vsize, hsize, CD_k=1):
        super().__init__()
        self.W = torch.nn.Parameter( torch.randn( hsize, vsize)*1e-2)
        self.bv = torch.nn.Parameter( torch.randn( vsize)*1e-2) # Bias V.
        self.bh = torch.nn.Parameter( torch.randn( hsize)*1e-2) # Bias H.
        self.k = CD_k # Divergencia Contrastiva.
        
    def sample_h( self, v):
        prob_h = torch.sigmoid( F.linear( v, self.W, self.bh))
        samp_h = torch.bernoulli( prob_h)
        return prob_h, samp_h

    def sample_v( self, h):
        prob_v = torch.sigmoid( F.linear( h, self.W.t(), self.bv))
        samp_v = torch.bernoulli( prob_v)
        return prob_v, samp_v

    def forward( self, v):
        vs = v
        for i in range( self.k):
            hp, hs = self.sample_h( vs)
            vp, vs = self.sample_v( hs)
            return v, vs

    def free_energy( self, v):
        v_bv = v.mv( self.bv) # Multiplica matriz por vector.
        hlin = torch.clamp( F.linear( v, self.W, self.bh), -80, 80)
        slog = hlin.exp().add(1).log().sum(1)
        return ( -slog - v_bv).mean()


################################################################
#		Rutina principal
################################################################

if __name__ == '__main__': # Para poder importar RBM.
    T = 20 # Cant de epocas.
    B = 64 # Mini-lotes.
    
    trn_data = tv.datasets.MNIST( # Entrenamiento.
    root='./data', train=True, download=True,
    transform=tv.transforms.ToTensor() )
    
    tst_data = tv.datasets.MNIST( # Validacion.
    root='./data', train=False, download=True,
    transform=tv.transforms.ToTensor() )
    
    trn_load = torch.utils.data.DataLoader(
    dataset=trn_data, batch_size=B, shuffle=True)
    
    tst_load = torch.utils.data.DataLoader(
    dataset=tst_data, batch_size=B, shuffle=False)

    # trn/tst-data/load como antes.
    N = 28*28 # Cant de entradas.
    M = 64 # Cant de features.
    C = 10 # Cant de clases de salida.
    rbmfd = RBM( N, M) # Feature Detector
    optim = torch.optim.SGD( rbmfd.parameters(), 0.1)
    for t in range(T):
        E = 0
        for images, labels in trn_load:
            optim.zero_grad()
            data = images.view( -1, N)
            v0, vk = rbmfd( data)
            loss = rbmfd.free_energy(v0) - rbmfd.free_energy(vk)
            loss.backward()
            optim.step()
            E += loss.item()
        print("RBM: ", t, E) 

    lincl = torch.nn.Linear( M, C) # Linear Classifier
    optim = torch.optim.SGD( lincl.parameters(), 0.1)
    costf = torch.nn.CrossEntropyLoss()
    for t in range(T):
        E = 0
        for images, labels in trn_load:
            optim.zero_grad()
            v = images.view( -1, N)
            hp, hs = rbmfd.sample_h( v)
            cp = lincl( hp)
            error = costf( cp, labels)
            error.backward()
            optim.step()
            E += error.item()
        print("Clasidicador lineal: ", t, E) 
            
    right = 0
    total = 0
    with torch.no_grad():
        for images, labels in tst_load:
            v = images.view( -1, N)
            hp, hs = rbmfd.sample_h( v)
            cp = lincl( hp)
            right += (cp.argmax(dim=1)==labels).sum().item()
            total += len(labels)
    print("Accuracy: ", right/total)


