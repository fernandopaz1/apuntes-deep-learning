import torch
import reviewer


reviews = reviewer.load( "imdb_labelled.txt")
wordlist, worddict = reviewer.vocabulary( reviews)
seqs = reviewer.sequence( reviews, worddict)

P = len(reviews)
N = len(wordlist)
T = 2


class SRNN( torch.nn.Module):
    def __init__( _, isize, hsize, osize):
        super().__init__()
        _.context_size = hsize
        #_.Wi = ...
        #_.Wc = ...
        #_.Wh = ...
        #_.Wo = ...

    def forward( _, x0, h0):
        # ...
        return x1, h1

    def predict( _, h):
        # ...
        return y

    def context( _, B=1):
        return torch.zeros( B, _.context_size)


def one_hot( p, N):
    assert( 0 <= p < N)
    pat = torch.zeros( 1, N)
    pat[ 0, p] = 1.
    return pat


model = SRNN( N, 200, 2)
optim = torch.optim.SGD( model.parameters(), lr=0.01)
costf = torch.nn.CrossEntropyLoss()

for t in range(T):
    E = 0.
    for b, (words, label) in enumerate(seqs):
        error = 0.
        h = model.context()
        optim.zero_grad()
        for i in range( len( words[:-1])):
            z = torch.tensor( words[i+1] ).view(1)
            x0 = one_hot( words[i], N)
            x1, h = model.forward( x0, h)
            error += costf( x1, z)
        y = model.predict( h)
        error += costf( y, torch.tensor(label))
        error.backward()
        optim.step()
        E += error.item()
        if b%100 == 0:
            print( t, b, error.item())
    print( E)
