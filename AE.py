import torch

class AE(torch.nn.Module):
    def __init__(_,vsize,hsize):   #vsize, tamaño de la entrada, hsize de la capa oculata, la salida tiene vsize
        super().__init__()
        _.L1 = torch.nn.Linear( vsize,hsize)
        _.L2 = torch.nn.Linear( hsize,vsize)
    
    
    def enc(_,x):                    #Encoder
        return torch.tanh(_.L1(x))   #aplica hace la transformacion lineal y aplica tanh
    
    def dec(_,x):                    #Decoder
        return torch.tanh(_.L2(h))   #Lo mismo que dec
    
    def forward(_,x):
        return _.dec(_.enc(x))
