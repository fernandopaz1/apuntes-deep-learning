import torch

class AE( torch.nn.Module):
	def __init__( _, vsize, hsize):
		super().__init__()
		_.enc = torch.nn.Sequential( torch.nn.Linear( vsize, hsize),torch.nn.Tanh())
		_.dec = torch.nn.Sequential( torch.nn.Linear( hsize, vsize),torch.nn.Tanh())

	def forward( _, x):
		return _.dec( _.enc( x)) # enc y dec se usan como funciones.
